# Alive.io

Alive.io is a game made during [Ludum Dare 46](http://ldjam.com/).

![title.png](Screenshots/title.png)

## Intro
Once upon a time there were some clans
God of Death wants them to fight and die. Self destruction is the nature anyway.
You, God of Life, want to keep them ALIVE!

But for how long?

## How to play?
*Rules:*
- You indirectly control the clans' behavior by placing food
- Try to keep them from killing each other as long as you can
- Or don't, just watch them fight!

*Controls:*
- Left click to spawn food
- Use mouse wheel to zoom the camera in and out
- Right click and drag to rotate the camera view
- There is useful info in HUD, you can also check the box "abundant food" to auto-generate, well, abundant, food.


## Links
[Ludum Dare 46](https://ldjam.com/events/ludum-dare/46/alive-io)
[itch.io](https://yidizhu.itch.io/aliveio)
[WebGL](http://www.yidizhu.com/resources/ld46/)

## Known Bugs
- If you look straight down from above, camera would glitch.

## P.S.
I really can't compose by myself so this game has no music. Enjoy the visual though!

## P.S. #2
Why is it called alive.io? A friend spotted that the colors I used in this game is very similar to those used in io games, and the mechanic of factions fight each other is similar too! So why not just name it io :p :stuck_out_tongue: 

## Credits
- [bfxr](https://www.bfxr.net/) was used for generating sound effects for this game.

## Copyright
Halite Games Studio