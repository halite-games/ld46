﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FoodManager : MonoBehaviour {

    public static FoodManager Instance;
    
    public List<Food> Foods = new List<Food>();

    [SerializeField] private AnimationCurve spawnFoodInterval = null;

    [SerializeField] private Vector2Int spawnFoodAmount = new Vector2Int(1, 4);

    [SerializeField] private GameObject foodPrefab = null;

    public int AvailableFood = 3;
    public float TimeUntilNextFood;
    private bool abundantFood = false;
    private bool shouldInstantSpawn = false;
    private float instantSpawnCD = 5;
    private float lastInstantSpawnTime = -100f;
    
    [SerializeField] private AnimationCurve foodRegenCD = null;
    
    public void RemoveFood(Food food) {
        Foods.Remove(food);
        foreach (Faction faction in FactionManager.Instance.AliveFactions) {
            faction.OnFoodRemoved(food);
        }
        
        Destroy(food.gameObject);
    }

    public bool HasFood() {
        return AvailableFood >= 1;
    }

    public void SpawnFoodAt(Vector3 position) {
        AvailableFood--;
        var food = Instantiate(foodPrefab, position, Quaternion.identity).GetComponent<Food>();
        food.PlaySonud();
        Foods.Add(food);
        FactionManager.Instance.OnFoodSpawned();
    }

    public bool CanPlaceFoodAt(Vector3 position) {
        bool canPlace = true;
        
        var factions = FactionManager.Instance.AliveFactions;
        foreach (Faction faction in factions) {
            float distance = Vector3.Distance(faction.transform.position, position);
            if (distance <= 1) {
                canPlace = false;
                break;
            }
        }

        foreach (Food food in Foods) {
            float distance = Vector3.Distance(food.transform.position, position);
            if (distance < 0.4f) {
                canPlace = false;
                break;
            }
        }

        return canPlace;
    }
    
    private void Awake() {
        Instance = this;
    }

    public void StartGame() {
        StartCoroutine(SpawnFoodCoroutine());
        StartCoroutine(RegenFoodCoroutine());
    }

    private IEnumerator RegenFoodCoroutine() {
        while (true) {
            float foodCD = foodRegenCD.Evaluate(Time.time);
            float startTime = Time.time;
        
            float elapsed = Time.time - startTime;
            while (elapsed < foodCD) {
                TimeUntilNextFood = foodCD - elapsed;    
                yield return null;
                elapsed = Time.time - startTime;
            }
            AvailableFood++;
        }

    }

    public void SetAutoSpawnFood(bool newAutoSpawn) {
        if (newAutoSpawn && !abundantFood) {
            if (Time.time - lastInstantSpawnTime > instantSpawnCD) {
                shouldInstantSpawn = true;
            }
        }

        abundantFood = newAutoSpawn;
    }
        
    private IEnumerator SpawnFoodCoroutine() {
        while (true) {
            int amount = abundantFood ? Random.Range(spawnFoodAmount.x, spawnFoodAmount.y) : 1;

            for (var i = 0; i < amount; i++) {
                Vector3 position;
                bool canPlaceFood;
                do {
                    position = new Vector3(Random.Range(-4f, 4f), 0f, Random.Range(-4f, 4f));
                    canPlaceFood = CanPlaceFoodAt(position);
                } while (!canPlaceFood);

                Foods.Add(Instantiate(foodPrefab, position, Quaternion.identity).GetComponent<Food>());
            }

            if (Foods.Count > 0) {
                Foods[Foods.Count - 1].PlaySonud();
                FactionManager.Instance.OnFoodSpawned();
            }
            
            float waitTime = spawnFoodInterval.Evaluate(Time.time);
            float waitStartTime = Time.time;
            yield return new WaitUntil(() => {
                bool waitedLongEnough = Time.time - waitTime >= waitStartTime;
                return waitedLongEnough || shouldInstantSpawn;
            });
            shouldInstantSpawn = false;
        }
    }
}
