using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public class Minion : MonoBehaviour {

    [SerializeField] private AudioClip[] explosionClips = null;
    [SerializeField] private GameObject explosionPrefab = null;
    [SerializeField] private GameObject body = null;
    private MeshRenderer bodyMesh = null;
    
    private enum State {
        Idle,
        Follow,
        Battle,
        Charge,
    }

    private Faction faction = null;
    
    [Header("Charge")]
    private Vector3 chargeDestination;
    private Vector3 afterChargeDirection;
    private float chargeSpeed;
    private float chargeDuration;
    private float chargeStartTime;

    [Header("Follow")] 
    private Vector3 desiredPosition;
    
    private State state = State.Follow;
    
    private bool isDeathCharging = false;

    public void Die() {
        state = State.Idle;
        StartCoroutine(DieCoroutine());
    }

    private IEnumerator DieCoroutine() {
        float fallDuration = 1;
        float fallStartTime = Time.time;

        while (Time.time - fallStartTime < fallDuration) {
            // Fall
            float elapsed = Time.time - fallStartTime;
            float t = elapsed / fallDuration;
            transform.localRotation = Quaternion.AngleAxis(90 * t, Vector3.forward);
            bodyMesh.material.SetColor("_Color", Color.Lerp(faction.factionColor, Color.grey, t));
            yield return null;
        }
        
        Destroy(gameObject);
    }

    public void SetDesiredPosition(Vector3 position) {
        desiredPosition = position;
    }

    public void DeathCharge(Vector3 position, float duration) {
        ChargeToPosition(position, duration, position);
        isDeathCharging = true;
    }

    public void ChargeToPosition(Vector3 position, float duration, Vector3 finalDirection) {
        chargeDestination = position;
        chargeDuration = duration / 2f;
        afterChargeDirection = finalDirection;
        chargeSpeed = Vector3.Distance(position, transform.position) / chargeDuration;
        chargeStartTime = Time.time;
        transform.LookAt(chargeDestination);
        state = State.Charge;
    }

    public void SetFaction(Faction faction) {
        this.faction = faction;
        bodyMesh = body.GetComponent<MeshRenderer>();
        bodyMesh.material.SetColor("_Color", faction.factionColor);
    }

    private void Start() {
        StartCoroutine(ShineCoroutine());
    }

    private IEnumerator ShineCoroutine() {
        float duration = 2;
        float startTime = Time.time;

        while (Time.time - startTime <= duration) {
            float elapsed = Time.time - startTime;
            float t = elapsed / duration;
            float sin = Mathf.Sin(t * 2 * Mathf.PI * 3);
            float sample = (sin + 1) / 2;
            
            bodyMesh.material.SetColor("_Color", Color.Lerp(faction.factionColor, Color.white, sample));
            yield return null;
        }
        
        bodyMesh.material.SetColor("_Color", faction.factionColor);
    }

    private void Update() {
        switch (state) {
            case State.Follow:
                transform.position = Vector3.Lerp(transform.position, desiredPosition, Time.deltaTime * 6f);
                transform.LookAt(transform.position + faction.transform.forward);
                // transform.LookAt(desiredPosition);
                break;
            case State.Charge:
                Vector3 direction = (chargeDestination - transform.position).normalized;
                if (float.IsInfinity(direction.x) || float.IsInfinity(direction.y) || float.IsInfinity(direction.z)) {
                    break;
                }
                // transform.LookAt(Vector3.Slerp(transform.forward, direction, Time.deltaTime));
                transform.Translate(direction * chargeSpeed * Time.deltaTime, Space.World);
                
                if (Time.time - chargeStartTime >= chargeDuration) {
                    transform.LookAt(transform.position + afterChargeDirection);
                    TransitToFollow();
                }
                Debug.DrawLine(transform.position + Vector3.up * 0.5f, chargeDestination + Vector3.up * 0.5f, Color.red);
                break;
            case State.Idle:
                
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (isDeathCharging) {
            if (Vector3.Distance(transform.position, chargeDestination) < 0.5f) {
                faction.opponent.TakeDamage(transform.position);
                
                // Explode!
                var explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
                var particles = explosion.GetComponent<ParticleSystem>();
                ParticleSystem.MainModule main = particles.main;
                main.startColor = faction.factionColor;
                
                var soundObject = new GameObject("Sound");
                soundObject.transform.position = transform.position;
                var source = soundObject.AddComponent<AudioSource>();
                source.spatialize = true;
                source.spatialBlend = 1;
                AudioClip clip = explosionClips[Random.Range(0, explosionClips.Length)];
                source.PlayOneShot(clip);
                
                Destroy(soundObject, clip.length + 0.5f);
                Destroy(explosion, 2f);
                Destroy(gameObject);
            }
        }
    }

    private void TransitToFollow() {
        state = State.Follow;
    }

    private void TransitToIdle() {
        state = State.Idle;
    }
}