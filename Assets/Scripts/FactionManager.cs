﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class FactionManager : MonoBehaviour {
    public static FactionManager Instance;
    [HideInInspector]
    public List<Faction> AliveFactions;

    public int InitialFactionCount = 3;
    
    [SerializeField] private GameObject factionPrefab = null;
    [SerializeField] private Color[] factionColors = null;

    [SerializeField] private float factionSpawnRadius = 2;
    [SerializeField] private float floating = 0.001f;

    public void OnFoodSpawned() {
        foreach (Faction faction in AliveFactions) {
            faction.OnFoodSpawned();
        }
    }

    public List<Faction> OtherFactions(Faction exclude) {
        var otherFactions = new List<Faction>();
        foreach (Faction faction in AliveFactions) {
            if (faction != exclude) {
                otherFactions.Add(faction);
            }
        }

        return otherFactions;
    }
    
    private void Awake() {
        Instance = this;
    }
    
    public void SpawnFactions() {
        for (var i = 0; i < InitialFactionCount; i++) {
            var color = factionColors[i];
            var newFaction = Instantiate(factionPrefab).GetComponent<Faction>();
            newFaction.factionColor = color;
            AliveFactions.Add(newFaction);
        }
        
        // Set faction positions
        for (int i = 0; i < AliveFactions.Count; i++) {
            bool isPointIntersecting;
            Vector3 position;
            
            do {
                position = GenerateRandomPosition();
                isPointIntersecting = false;
            
                for (var j = 0; j < i; ++j) {
                    float distance = Vector3.Distance(position, AliveFactions[j].transform.position);
                    if (distance < factionSpawnRadius) {
                        isPointIntersecting = true;
                        break;
                    }
                }

            } while (isPointIntersecting);

            AliveFactions[i].transform.position = position;
        }
    }

    private Vector3 GenerateRandomPosition() {
        float range = 4;
        return new Vector3(Random.Range(-range, range), floating, Random.Range(-range, range));
    }
}
