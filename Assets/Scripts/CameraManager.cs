﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraManager : MonoBehaviour {

    [SerializeField] private Color placeableColor = Color.clear;
    [SerializeField] private Color blockedColor = Color.clear;
    
    [SerializeField] private GameObject foodVisualizePrefab = null;
    
    [SerializeField]
    private Vector3 lookAtTarget = Vector3.zero;

    [SerializeField]
    private Vector3 pointOnUnitSphere = new Vector3(0, 0.5f, -0.5f);

    [SerializeField] private float distance = 1;

    [SerializeField] private float minDistance = 5;

    [SerializeField] private float maxDistance= 15;

    [SerializeField] private float distancePerScroll = 1;

    [SerializeField] private float anglePerPixel = 1;

    private GameObject foodVisualizer;
    private MeshRenderer foodMesh;
    
    private Camera mainCamera;
    
    private Vector3 lastFrameMousePosition;

    private void Start() {
        foodVisualizer = Instantiate(foodVisualizePrefab, Vector3.down * 10, Quaternion.identity);
        foodMesh = foodVisualizer.transform.GetChild(0).GetComponent<MeshRenderer>();
        mainCamera = GetComponent<Camera>();
    }

    private void Update() {
        if (!UIManager.Instance.IsInGame) {
            return;
        }
        
        pointOnUnitSphere.Normalize();

        distance += -distancePerScroll * Input.mouseScrollDelta.y;

        if (Input.GetMouseButton(1)) {
            // mouse drag
            Vector3 delta = Input.mousePosition - lastFrameMousePosition;
            Quaternion yRotation = Quaternion.AngleAxis(delta.x * anglePerPixel * Mathf.Deg2Rad, Vector3.up);
            Quaternion xRotation = Quaternion.AngleAxis(-delta.y * anglePerPixel * Mathf.Deg2Rad, transform.right);
            pointOnUnitSphere = yRotation * xRotation * pointOnUnitSphere;
        }

        pointOnUnitSphere.Normalize();
        distance = Mathf.Clamp(distance, minDistance, maxDistance);
        
        transform.position = pointOnUnitSphere * distance + lookAtTarget;
        transform.LookAt(lookAtTarget);

        VisualizeFoodPlacement();
        
        lastFrameMousePosition = Input.mousePosition;
    }

    private void VisualizeFoodPlacement() {
        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);

        Debug.DrawRay(ray.origin, ray.origin + ray.direction * 100, Color.red);

        var plane = new Plane(Vector3.up, Vector3.zero);
        plane.Raycast(ray, out float enter);
        Vector3 hitPoint = ray.origin + ray.direction * enter;
        bool isHit = hitPoint.x < 5f && hitPoint.x > -5f && hitPoint.z < 5f && hitPoint.z > -5f;

        if (isHit) {
            bool canPlace = FoodManager.Instance.CanPlaceFoodAt(hitPoint) && FoodManager.Instance.HasFood();

            foodVisualizer.transform.position = hitPoint;
            foodMesh.material.SetColor("_Color", canPlace ? placeableColor : blockedColor);

            if (canPlace && Input.GetMouseButtonDown(0)) {
                FoodManager.Instance.SpawnFoodAt(hitPoint);
            }
        } else {
            foodVisualizer.transform.position = new Vector3(0, -100, 0);
        }
    }
}
