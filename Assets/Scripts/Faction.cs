using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;
// ReSharper disable Unity.InefficientPropertyAccess

internal enum FactionState {
    MoveToFood,
    Turn,
    War,
    CivilWar,
    Defend,
    Idle,
    Die,
    
}

public class Faction : MonoBehaviour {
    [Header("Sound")] 
    [SerializeField] private AudioClip pickupClip = null;

    [SerializeField] private AudioClip dieClip = null;
    [SerializeField] private AudioClip[] attackClips = null;
    
    [SerializeField] public Color factionColor = Color.green;
    
    [SerializeField] private GameObject heart = null;
    [SerializeField] private GameObject plate = null;

    [Header("War")] 
    [SerializeField] private float guardRadius = 3;
    
    private float warStartTime;
    private float warDuration = 1f;
    private float warCoolDown = 4f;
    private float warFinishTime;
    public Faction opponent;

    [Header("Minion")]
    [SerializeField] private GameObject minionPrefab = null;
    [SerializeField] private int initialMinionCount = 8;
    public float minionRadius = 0.5f;
    
    public List<Minion> minions = new List<Minion>();
    
    [Header("Movement")]
    [SerializeField] private float speed = 0.8f;
    private float moveSpeed => speed * UIManager.CurrentModifier();
    
    [SerializeField] private float stoppingDistance = 0.1f;
    [SerializeField] private Vector2 wayPointDistanceRange = new Vector2(1, 3);
    private Food foodTarget = null;
    
    [Header("Rotation")] 
    [SerializeField] private float rotationSpeed = 150;
    private float spicedRotationSpeed => rotationSpeed * UIManager.CurrentModifier();
    
    [Header("Idle")] 
    [SerializeField] private float idleDuration = 3;
    private float idleStartTime = 0;
    
    [Header("Buffs")] // get stronger when eaten a lot
    // OR get stronger when army is bigger?
    private float speedModifier;
    
    private FactionState state = FactionState.Idle;

    private AudioSource source;
    
    private static readonly int color = Shader.PropertyToID("_Color");

    public void TakeDamage(Vector3 from) {
        if (minions.Count > 0) {
            Minion nearestMinion = null;
            float nearestDistance = float.MaxValue;

            foreach (Minion minion in minions) {
                float distance = Vector3.Distance(minion.transform.position, from);
                if (distance < nearestDistance) {
                    nearestDistance = distance;
                    nearestMinion = minion;
                }
            }

            nearestMinion.Die();
            StartCoroutine(ShineCoroutine(Color.red, 1, 0.5f));

            minions.Remove(nearestMinion);
        }

        if (minions.Count == 0) {
            Die();
        }
        
    }

    private void Die() {
        state = FactionState.Die;
        FactionManager.Instance.AliveFactions.Remove(this);
        StartCoroutine(DieCoroutine());
    }

    private IEnumerator DieCoroutine() {
        float duration = 2;
        float startTime = Time.time;
        float elapsed = Time.time - startTime;

        source.spatialBlend = 0;
        source.PlayOneShot(dieClip);
        var renderers = new List<MeshRenderer>();
        renderers.Add(plate.GetComponent<MeshRenderer>());
        renderers.Add(heart.GetComponent<MeshRenderer>());
        
        while (elapsed <= duration) {
            float t = elapsed / duration;

            foreach (MeshRenderer meshRenderer in renderers) {
                meshRenderer.material.SetColor(color, Color.Lerp(factionColor, Color.grey, t));
            }
            
            yield return null;
            elapsed = Time.time - startTime;
        }
        
        foreach (MeshRenderer meshRenderer in renderers) {
            meshRenderer.material.SetColor(color, Color.grey);
        }
        GetComponent<CapsuleCollider>().radius = 0.5f;
        heart.GetComponent<Heart>().StopBeating();
        gameObject.layer = LayerMask.NameToLayer("Default");
        
        UIManager.Instance.OnOneFactionDied();
        
        while (true) {
            state = FactionState.Die;
            foreach (Minion minion in minions) {
                Destroy(minion);
            }
            minions.Clear();
            yield return null;
        }
    }
    
    public void OnFoodRemoved(Food food) {
        if (food == foodTarget) {
            foodTarget = null;
            // TransitToMoveToNearestFood();
            TransitToIdle();
        }
    }
    
    public void OnFoodSpawned() {
        if (state == FactionState.MoveToFood) {
            float timeToTarget = Vector3.Distance(foodTarget.transform.position, transform.position) / moveSpeed;
            var nearestFood = FindNearestFood();
            if (nearestFood.shortestTime < timeToTarget) {
                SetFoodTarget(nearestFood.nearestFood);
                TransitToTurn(FactionState.MoveToFood, foodTarget.transform.position);
            }
        }
    }
    
    private void Start() {
        
        heart.GetComponent<MeshRenderer>().material.SetColor(color, factionColor);
        plate.GetComponent<MeshRenderer>().material.SetColor(color, factionColor);
        source = GetComponent<AudioSource>();
        
        InitializeMinions();
        // TransitToTurn(FactionState.Move);
        TransitToIdle();
    }

    private void InitializeMinions() {
        for (var i = 0; i < initialMinionCount; i++) {
            SpawnMinion();
        }
    }

    private void SpawnMinion() {
        var minion = Instantiate(minionPrefab).GetComponent<Minion>();
        minion.transform.position = transform.position;
        minion.SetFaction(this);
            
        minions.Add(minion);
    }

    private void Update() {

        switch (state) {
            case FactionState.MoveToFood:
                if (foodTarget == null) {
                    TransitToIdle();
                    break;
                }
                TickMove();
                
                if (Vector3.Distance(transform.position, foodTarget.transform.position) <= stoppingDistance) {
                    ConsumeFood(foodTarget);
                    TransitToMoveToNearestFood();
                }
                break;
            case FactionState.Turn:
                
                break;
            case FactionState.Idle:
                ComputeMinionPositions(true);
                
                if (Time.time - idleStartTime >= idleDuration) {
                    TransitToMoveToNearestFood();
                }
                break;

            case FactionState.War:
                transform.LookAt(opponent.transform.position);
                UpdateMinionPositions();
                
                Debug.DrawLine(transform.position + Vector3.up * 0.25f, opponent.transform.position + Vector3.up * 0.25f, Color.red);

                if (Time.time - warStartTime >= warDuration) {
                    warFinishTime = Time.time;
                    TransitToIdle();
                }
                break;
            case FactionState.CivilWar:
                break;
            case FactionState.Defend:
                break;
            case FactionState.Die:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void SetFoodTarget(Food food) {
        foodTarget = food;
    }
    
    private void ConsumeFood(Food food) {
        SpawnMinion();
        source.PlayOneShot(pickupClip);
        foodTarget = null;
        FoodManager.Instance.RemoveFood(food);
    }
    
    private struct NearestFood {
        public Food nearestFood;
        public float shortestTime;

        public NearestFood(GameObject dummy) {
            nearestFood = null;
            shortestTime = float.MaxValue;
        }
    }

    private void TransitToMoveToNearestFood() {
        NearestFood target = FindNearestFood();
        if (target.nearestFood != null) {
            SetFoodTarget(target.nearestFood);
            TransitToTurn(FactionState.MoveToFood, foodTarget.transform.position);
        } else {
            TransitToIdle();
        }
    }
    
    private NearestFood FindNearestFood() {
        var foods = FoodManager.Instance.Foods;

        var nearestFood = new NearestFood(null);
        
        foreach (Food food in foods) {
            float distance = Vector3.Distance(food.transform.position, transform.position);
            float angle = Vector3.Angle(transform.forward, food.transform.position - transform.position);
            float rotationTime = angle / spicedRotationSpeed;
            float moveTime = distance / moveSpeed;
            float totalTime = moveTime + rotationTime;
            if (totalTime < nearestFood.shortestTime) {
                nearestFood.nearestFood = food;
                nearestFood.shortestTime = totalTime;
            }
        }

        return nearestFood;
    }
    
    private void TickMove() {
        Vector3 targetPosition = foodTarget.transform.position;
        Vector3 position = transform.position;
        Vector3 moveDir = (targetPosition - position).normalized;

        transform.position += moveSpeed * Time.deltaTime * moveDir;
        
        transform.LookAt(position + moveDir);
        UpdateMinionPositions();
        
        Debug.DrawLine(position + Vector3.up * 0.5f, targetPosition + Vector3.up * 0.5f, factionColor);

        if (Time.time - warFinishTime > warCoolDown) {
            var offset = Vector3.up * 0.5f;
            Debug.DrawLine(transform.position + offset, transform.position + transform.forward * guardRadius + offset, Color.red);
            Debug.DrawLine(transform.position + offset, transform.position + transform.right * guardRadius + offset, Color.red);
            Debug.DrawLine(transform.position + offset, transform.position + -transform.right * guardRadius + offset, Color.red);
            Debug.DrawLine(transform.position + offset, transform.position + -transform.forward * guardRadius + offset, Color.red);
        
            foreach (Faction faction in FactionManager.Instance.OtherFactions(this)) {
                float distance = Vector3.Distance(faction.transform.position, transform.position);
                if (distance <= guardRadius) {
                    opponent = faction;
                    TransitToTurn(FactionState.War, opponent.transform.position);
                    break;
                }
            }
        }
    }

    private void TransitToTurn(FactionState nextState, Vector3 targetPosition) {
        state = FactionState.Turn;
        StartCoroutine(TurnCoroutine(nextState, targetPosition));
    }
    
    private void TransitToMove() {
        state = FactionState.MoveToFood;
    }

    private void TransitToWar() {
        state = FactionState.War;
        warStartTime = Time.time;
        foodTarget = null;

        if (minions.Count > 1) {
            Minion minion = minions[0];
            minion.DeathCharge(opponent.transform.position, 0.75f);
            minions.RemoveAt(0);
            StartCoroutine(ShineCoroutine(Color.white));
            source.PlayOneShot(attackClips[Random.Range(0, attackClips.Length)]);
        }
    }

    private IEnumerator ShineCoroutine(Color shineColor, int repetition = 1, float duration = 1) {
        MeshRenderer heartMesh = heart.GetComponent<MeshRenderer>();
        
        float startTime = Time.time;

        while (Time.time - startTime <= duration) {
            float elapsed = Time.time - startTime;
            float t = elapsed / duration;
            float sin = Mathf.Sin(t * 2 * Mathf.PI * repetition);
            float sample = (sin + 1) / 2;
            
            heartMesh.material.SetColor("_Color", Color.Lerp(factionColor, shineColor, sample));
            yield return null;
        }
        
        heartMesh.material.SetColor("_Color", factionColor);
    }
    
    private void TransitToIdle() {
        state = FactionState.Idle;
        idleStartTime = Time.time;
        UpdateMinionPositions();
    }

    private IEnumerator TurnCoroutine(FactionState nextState, Vector3 targetPosition) {
        Vector3 targetDir = (targetPosition - transform.position).normalized;
        Vector3 startDir = transform.forward;

        float duration = Vector3.Angle(targetDir, startDir) / spicedRotationSpeed;
        float startTime = Time.time;
        float elapsedTime = Time.time - startTime;

        // Look away temporarily to calculate target minion positions
        Vector3 currentForward = transform.forward;
        transform.LookAt(targetPosition);
        var positions = ComputeMinionPositions();
        for (var i = 0; i < minions.Count; ++i) {
            minions[i].ChargeToPosition(positions[i], duration, (targetPosition - transform.position).normalized);
            minions[i].SetDesiredPosition(positions[i]);
        }
        transform.LookAt(transform.position + currentForward);
        
        while (elapsedTime <= duration) {
            transform.LookAt(transform.position + Vector3.Slerp(startDir, targetDir, elapsedTime / duration));
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        
        transform.LookAt(transform.position + targetDir);

        if (nextState == FactionState.MoveToFood) {
            TransitToMove();
        } else if (nextState == FactionState.War) {
            TransitToWar();    
        }
    }

    private Vector3 NextWayPoint() {
        //TODO: Implement other way point selection criteria here, like environment, enemies
        Vector3 nextWayPoint;

        RaycastHit info;
        do {
            float angle = Random.value * 360f;
            var direction = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle));
            float distance = Random.Range(wayPointDistanceRange.x, wayPointDistanceRange.y);
            Vector3 position = transform.position;
            nextWayPoint = position + distance * direction;

            Physics.Raycast(position, direction, out info, minionRadius);
        } while (info.collider && info.collider.CompareTag("Obstacle"));
        
        return nextWayPoint; 
    }

    private Vector3[] ComputeMinionPositions(bool useRandom = false) {
        int minionCount = minions.Count;
        var positions = new Vector3[minionCount];

        float angleInterval = 360.0f / minionCount;
        
        for (var i = 0; i < minionCount; i++) {
            float angle = angleInterval * i;
            float angleInDeg = angle * Mathf.Deg2Rad;
            var direction = new Vector3(Mathf.Sin(angleInDeg), 0, Mathf.Cos(angleInDeg));
            Vector3 worldDirection = transform.TransformDirection(direction);
            Vector3 position = transform.position + worldDirection * minionRadius;
            if (useRandom) {
                const float SPICE = 0.15f;
                position += new Vector3(Random.value * SPICE, 0, Random.value * SPICE);
            }
            
            positions[i] = position;
        }
        
        return positions;
    }

    private void UpdateMinionPositions() {
        var positions = ComputeMinionPositions();
        for (var i = 0; i < minions.Count; ++i) {
            minions[i].SetDesiredPosition(positions[i]);
        }
    }
}