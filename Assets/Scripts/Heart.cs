﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heart : MonoBehaviour {
    [SerializeField] private float spinSpeed = 60;

    private bool stopped = false;
        
    public void StopBeating() {
        stopped = true;
    }
    private void Update() {
        if (!stopped) {
            transform.Rotate(Vector3.up, spinSpeed * Time.deltaTime, Space.World);
            transform.Rotate(Vector3.right, spinSpeed * Time.deltaTime, Space.World);
        }
        
    }
}
