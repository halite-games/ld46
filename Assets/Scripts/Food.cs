﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Food : MonoBehaviour {

    [SerializeField] private AudioClip[] clips = null;
    
    public void PlaySonud() {
        GetComponent<AudioSource>().PlayOneShot(clips[Random.Range(0, clips.Length)]);
    }
}
