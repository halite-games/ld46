﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {
    public static UIManager Instance;

    public AnimationCurve IntensityModifier = null;
    
    [SerializeField] private GameObject welcomeCanvas = null;
    [SerializeField] private TextMeshProUGUI factionCountText = null;
    [SerializeField] private Slider factionCountSlider = null;
    
    [SerializeField] private GameObject inGameCanvas = null;
    [SerializeField] private TextMeshProUGUI foodCountText = null;
    [SerializeField] private TextMeshProUGUI foodCDText = null;
    [SerializeField] private Toggle autoSpawnFoodToggle = null;

    [SerializeField] private GameObject onOneFactionDiedCanvas = null;
    [SerializeField] private TextMeshProUGUI aliveTimeText = null;
    
    [SerializeField] private GameObject afterOneFactionDiedCanvas = null;
    [SerializeField] private TextMeshProUGUI timeText = null;


    public bool IsInGame = false;
    
    private FoodManager foodManager;
    private float gameStartTime;

    private float Elapsed => Time.time - gameStartTime;

    public static float CurrentModifier() {
        return Instance.IntensityModifier.Evaluate(Time.time);
    }
    
    private void Awake() {
        Instance = this;
    }

    private void Start() {
        foodManager = FoodManager.Instance;
        welcomeCanvas.SetActive(true);
        inGameCanvas.SetActive(false);
        onOneFactionDiedCanvas.SetActive(false);
        afterOneFactionDiedCanvas.SetActive(false);
    }

    private void Update() {
        if (welcomeCanvas.activeSelf) {
            factionCountText.text = $"{factionCountSlider.value}";
            FactionManager.Instance.InitialFactionCount = (int) factionCountSlider.value;
        }
        
        if (inGameCanvas.activeSelf) {
            foodCountText.text = $"Available Food: <color=#58FF73>{foodManager.AvailableFood}</color>";
            foodCDText.text = $"Next Food In: <color=#58FF73>{foodManager.TimeUntilNextFood:0.##}</color>s";
            FoodManager.Instance.SetAutoSpawnFood(autoSpawnFoodToggle.isOn);
        }

        if (afterOneFactionDiedCanvas.activeSelf) {
            timeText.text = $"<color=#58FF73>{Elapsed:0.##}</color>s";
        }
    }

    public void StartGame() {
        gameStartTime = Time.time;
        Time.timeScale = 1;
        FactionManager.Instance.SpawnFactions();
        FoodManager.Instance.StartGame();
        inGameCanvas.SetActive(true);
        welcomeCanvas.SetActive(false);
        IsInGame = true;
    }
    
    public void OnOneFactionDied() {
        if (afterOneFactionDiedCanvas.activeSelf) {
            return;
        }
        
        Time.timeScale = 0;
        inGameCanvas.SetActive(false);
        onOneFactionDiedCanvas.SetActive(true);
        aliveTimeText.text = $"Looks like one of the clans is ELIMINATED!\n" + 
                             $"You kept them alive for <color=#58FF73>{Elapsed:0.##}</color> seconds.\n" + 
                             $"Not bad as a god.";
    }

    public void Resume() {
        onOneFactionDiedCanvas.SetActive(false);
        inGameCanvas.SetActive(true);
        afterOneFactionDiedCanvas.SetActive(true);
        Time.timeScale = 1;
    }
    
    public void Reload() {
        SceneManager.LoadScene("SampleScene");
    }
}
